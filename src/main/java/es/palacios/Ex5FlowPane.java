package es.palacios;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Ex5FlowPane extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        FlowPane flowPane = new FlowPane();
        for (int i = 0; i < 7; i++) {
            VBox vBox = createVbox();
            flowPane.getChildren().add(vBox);
        }
        Scene scene = new Scene(flowPane, 800, 600);
        stage.setTitle("Flow Pane");
        stage.setScene(scene);
        stage.show();
    }

    public VBox createVbox() {
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(20,20,20,20));
        vBox.setStyle("-fx-border-color:black;\n");
        Label label = new Label("Etiqueta VBox");
        Button button1 = new Button("Button 1 VBox");
        button1.setMinSize(125,125);
        Button button2 = new Button("Button 2 VBox");
        TextField textField = new TextField("TextField VBox");
        RadioButton radioButton = new RadioButton("RadioButton VBox");
        CheckBox checkBox = new CheckBox("CheckBox VBox");

        vBox.getChildren().addAll(label,button1,button2,textField,radioButton,checkBox);
        return vBox;
    }

    public static void main(String[] args) {
        launch(args);
    }

}
