package es.palacios;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Ex4HBox extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(20,20,20,20));
        Label label = new Label("Etiqueta HBox");
        Button button1 = new Button("Button 1 HBox");
        button1.setMinSize(125,125);
        Button button2 = new Button("Button 2 HBox");
        TextField textField = new TextField("TextField HBox");
        RadioButton radioButton = new RadioButton("RadioButton HBox");
        CheckBox checkBox = new CheckBox("CheckBox HBox");
        hBox.getChildren().addAll(label,button1,button2,textField,radioButton,checkBox);

        Scene scene = new Scene(hBox, 800, 300);
        stage.setTitle("Ex4 - HBox");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}

