module es.palacios.diu2t3 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;


    opens es.palacios to javafx.fxml;
    exports es.palacios;
}